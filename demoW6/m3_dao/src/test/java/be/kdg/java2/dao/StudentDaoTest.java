package be.kdg.java2.dao;

import be.kdg.java2.model.Student;
import org.junit.jupiter.api.*;

import java.time.LocalDate;

class StudentDaoTest {
    private static StudentDao myDao;

    @BeforeAll
    public static void firstOfAll() {
        myDao = new StudentDao();
    }

    @BeforeEach
    public void init() {
        myDao.voegToe(new Student(9999, "Charlotte Vermeulen", LocalDate.of(2000, 1, 24), "Antwerpen"));
        myDao.voegToe(new Student(666, "Donald Trump", LocalDate.of(1946, 6, 14), "Washington"));
        myDao.voegToe(new Student(123, "Sam Gooris", LocalDate.of(1973, 4, 10), "Antwerpen"));
        myDao.voegToe(new Student(333, "Koen Schram", LocalDate.of(1967, 5, 15), "Merksem"));
    }

    @AfterEach
    public void teardown() {
        myDao.deleteAll();
    }

    @AfterAll
    public static void destroy() {
        myDao.close();
    }

    //TODO schrijf testToevoegen

    //TODO schrijf testVerwijderen

}