
import be.kdg.java2.model.Student;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class SerializeDemo {
    public static void main(String[] args) {
        List<Student> studentList = new ArrayList<>();

        studentList.add(new Student(9999, "Charlotte Vermeulen", LocalDate.of(2000, 1, 24), "Antwerpen"));
        studentList.add(new Student(666, "Donald Trump", LocalDate.of(1946, 6, 14), "Washington"));
        studentList.add(new Student(123, "Sam Gooris", LocalDate.of(1973, 4, 10), "Antwerpen"));
        studentList.add(new Student(333, "Koen Schram", LocalDate.of(1967, 5, 15), "Merksem"));

        System.out.println("Voor serialize:");
        studentList.forEach(System.out::println);

        try {
            ObjectOutputStream oos = new ObjectOutputStream(
                    new FileOutputStream("data/studenten.ser"));
            //TODO schrijf de ArrayList weg


            studentList.clear();
            ObjectInputStream ois = new ObjectInputStream(
                    new FileInputStream("data/studenten.ser"));
            //TODO lees de ArrayList terug in

            System.out.println("\nNa serialize / deserialize:");
            studentList.forEach(System.out::println);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
