package be.kdg.java2.model;

import java.time.LocalDate;

public class Student {
    private int studNr;
    private String naam;
    private LocalDate geboorte;
    private String woonplaats;

    public Student(int studNr, String naam, LocalDate geboorte, String woonplaats) {
        this.studNr = studNr;
        this.naam = naam;
        this.geboorte = geboorte;
        this.woonplaats = woonplaats;
    }

    @Override
    public String toString() {
        return String.format("%-6d %-20s (°%s) --> %s", studNr, naam, geboorte, woonplaats);
    }
}