
import be.kdg.java2.model.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * DISCLAIMER: Puur omwille van de demo doen we in de main alles achter elkaar:
 * Connection maken, tabel aanmaken, queries uitvoeren, data ophalen en database sluiten.
 *
 * In een volgende fase gaan we het DAO-pattern toepassen en alle databaseverrichtingen
 * in een aparte dao-klasse steken
 */

public class JDBCDemo {
    public static void main(String[] args) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            // STAP 1. Maak een connection met de database
            connection = DriverManager.getConnection("jdbc:hsqldb:file:data/studenten", "sa", "");
            System.out.println("Connection OK");

            // STAP 2. Maak een statement klaar
            statement = connection.createStatement();
            statement.execute("DROP TABLE IF EXISTS studenten");
            String createQuery = "CREATE TABLE studenten " +
                    "(id INTEGER NOT NULL IDENTITY," +
                    "studnr INTEGER NOT NULL, " +
                    "naam VARCHAR(30) NOT NULL," +
                    "geboorte DATE," +
                    "woonplaats VARCHAR(40))";
            statement.execute(createQuery);
            System.out.println("Tabel aangemaakt");

            // STAP3. Execute queries
            //TODO: zoek hieronder het foutje in een van de queries:
            String query1 = "INSERT INTO studenten VALUES (NULL, 111, 'Kasper', '"
                    + Date.valueOf("2000-8-15") + "', 'Berchem')";
            int rowsAffected = statement.executeUpdate(query1);
            String query2 = "INSERT INTO studenten VALUES (NULL, 222, 'Melchior', '"
                    + Date.valueOf("1999-10-19") + "', 'Antwerpen')";
            rowsAffected += statement.executeUpdate(query2);
            String query3 = "INSERT INTO studenten VALUES (NULL, 333, 'Balthazar', '"
                    + Date.valueOf("2001-3-2") + "', 'Kortrijk')";
            rowsAffected += statement.executeUpdate(query3);
            System.out.println("Na insert: rowsAffected: " + rowsAffected);

            //TODO: Verwijder alle studenten die in Antwerpen wonen

            //TODO: Verander de naam van het eerste studentenrecord in je eigen naam

            // STAP 4. Verwerk de opgehaalde data
            List<Student> myList = new ArrayList<>();
            resultSet = statement.executeQuery("SELECT * FROM studenten");
            //TODO: zet de resultSet om naar myList

            System.out.println("opgehaalde data:");
            myList.forEach(System.out::println);

            // STAP 5. Verbreek de verbinding met de database
            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState = " + e.getSQLState());
            System.out.println("Error code = " + e.getErrorCode());
        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (statement != null) statement.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}


